import com.orangapps.gpindexer.fetcher.helpers.GooglePlayPageLoader

beans {
    xmlns context: "http://www.springframework.org/schema/context"
    context.'component-scan'('base-package': "com.orangapps.gpindexer.fetcher.helpers.parse")

    googlePlayPageLoader(GooglePlayPageLoader)  {

    }

}