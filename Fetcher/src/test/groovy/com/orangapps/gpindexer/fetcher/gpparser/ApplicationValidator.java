package com.orangapps.gpindexer.fetcher.gpparser;


import com.orangapps.persistence.persist.sql.domain.Application;

/**
 *
 */
public class ApplicationValidator {
    public static boolean validate(Application app) {
        return app != null && app.getAndroidVersion() != null
                && app.getName() != null
                && app.getCategory() != null
                && app.getContentRaiting() != null
                && app.getDescription() != null
                && app.getInstallationCount() != null
                && app.getLastUpdateDate() != null
                && app.getLink() != null
                && app.getPackageName() != null
                && app.getSeller() != null;
    }
}
