package com.orangapps.gpindexer.fetcher.helpers

import com.orangapps.gpindexer.GenericGroovyContextLoader
import com.orangapps.gpindexer.fetcher.gpparser.AppPackagesFetcher
import com.orangapps.gpindexer.fetcher.helpers.parse.AppParser
import org.apache.commons.io.IOUtils
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.transaction.TransactionConfiguration
import spock.lang.Specification

/**
 * Created by rurik on 01.02.15.
 */

@ContextConfiguration(locations = "classpath:test-config.groovy",
        loader = GenericGroovyContextLoader.class)
@TransactionConfiguration
class AppsFinderSpec extends Specification {
    def SEARCH_APP_HTML = "/searchpage/testsearch.htm"
    def PACKAGES_FILE = "/searchpage/testsearch_packages.txt"

    AppsFinder appsFinder = new AppsFinder()

    @Autowired
    AppParser appParser;

    @Autowired
    GooglePlayPageLoader gpPageLoader;

    def "Offline: Parsing of search page works fine"() {
        when:
        String html = IOUtils.toString(this.getClass().getResourceAsStream(SEARCH_APP_HTML), "UTF-8")

        String packagesString = IOUtils.toString(this.getClass().getResourceAsStream(PACKAGES_FILE), "UTF-8")
        List packages = packagesString.split("\n").toList()
        Document page = Jsoup.parse(html)
        Set<String> appsOnPage = new HashSet<>(appsFinder.findAppsOnPage(page))
        def size = appsOnPage.size()

        then:
        size > 0
        packages.containsAll(appsOnPage)
    }

    def "Online: Parsing of search page works fine"() {
        when:
        String url = AppPackagesFetcher.urlToSearch.get(0)
        Document page = gpPageLoader.downloadPage(url);
        def appsOnPage = appsFinder.findAppsOnPage(page)

        then:
        appsOnPage.size() > 0
    }


}
