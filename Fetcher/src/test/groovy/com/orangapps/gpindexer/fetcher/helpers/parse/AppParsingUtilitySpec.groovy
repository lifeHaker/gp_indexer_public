package com.orangapps.gpindexer.fetcher.helpers.parse

import spock.lang.Specification

import static com.orangapps.gpindexer.fetcher.helpers.parse.AppParsingUtility.getDecimalApkSize
import static com.orangapps.gpindexer.fetcher.helpers.parse.AppParsingUtility.getPackageFromUrl

/**
 * Created by rurik on 01.02.15.
 */

class AppParsingUtilitySpec extends Specification {

    public static final String URL_TEMPLATE = "https://play.google.com/store/apps/details?hl=ru&id="

    def "GoogleParser getPackage works fine"() {
        expect:
        getPackageFromUrl(url) == packageName

        where:
        url                                     || packageName
        URL_TEMPLATE + "com.qless.consumer.app" || "com.qless.consumer.app"
    }

    def "GoogleParser getDecimalApkSize works fine"() {
        expect:
        getDecimalApkSize(str) == size

        where:
        str                     || size
        "8.2M"                  || 8.2
        "20M"                   || 20
        "644k"                  || 0.644
        "3,8M"                  || 3.8
        "Varies with device"    || Integer.MAX_VALUE
        "Зависит от устройства" || Integer.MAX_VALUE
        ""                      || 0
    }

}
