package com.orangapps.gpindexer.fetcher.helpers

import com.orangapps.gpindexer.GenericGroovyContextLoader
import com.orangapps.gpindexer.fetcher.helpers.parse.AppParser
import com.orangapps.persistence.persist.sql.domain.Application
import org.apache.commons.io.IOUtils
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.transaction.TransactionConfiguration
import spock.lang.Specification

import static com.orangapps.gpindexer.fetcher.gpparser.AppsFetcher.GOOGLE_PLAY_PREFFIX
import static com.orangapps.gpindexer.fetcher.gpparser.AppsFetcher.STORE_APPS_DETAILS_ID

/**
 * Created by rurik on 01.02.15.
 */

@ContextConfiguration(locations = "classpath:test-config.groovy",
        loader = GenericGroovyContextLoader.class)
@TransactionConfiguration
class AppsFetcherSpec extends Specification {
    def TEST_APP_HTML = "/apppage/testApp.htm"

    @Autowired
    AppParser appParser;

    @Autowired
    GooglePlayPageLoader gpPageLoader;

    def "Offline: Parsing of application page works fine"() {
        when:
        String html = IOUtils.toString(this.getClass().getResourceAsStream(TEST_APP_HTML), "UTF-8")
        Document page = Jsoup.parse(html)
        Application application = appParser.getApplicationInfo(page);

        then:
        application.name == "Мой Говорящий Том"
        application.packageName == "com.outfit7.mytalkingtomfree"
        application.category == "Казуальные"
        application.score == 4.4d
        application.installationCount == "100 000 000–500 000 000"
        application.size == "61M"
        !application.screenShots.isEmpty()
        application.getLogoImage()!=null
        !application.getLogoImage().getGooglePlayLink().isEmpty()
        application.lastUpdateDate == "15 февраля 2016 г."
    }

    def "Online: Parsing of application page works fine"() {
        when:
        String url = GOOGLE_PLAY_PREFFIX + STORE_APPS_DETAILS_ID + "com.outfit7.mytalkingtomfree";
        Document page = gpPageLoader.downloadPage(url);
        Application application = appParser.getApplicationInfo(page);

        then:
        application.name == "Мой Говорящий Том"
        application.packageName == "com.outfit7.mytalkingtomfree"
        application.category == "Казуальные"
        application.score == 4.4d
        application.installationCount == "100 000 000–500 000 000"
        application.size == "61M"
        !application.screenShots.isEmpty()
        application.getLogoImage()!=null
        !application.getLogoImage().getGooglePlayLink().isEmpty()
        application.lastUpdateDate == "15 февраля 2016 г."
    }
}
