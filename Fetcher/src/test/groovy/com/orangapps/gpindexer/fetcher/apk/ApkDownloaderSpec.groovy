package com.orangapps.gpindexer.fetcher.apk

import spock.lang.IgnoreIf
import spock.lang.Specification

import static com.orangapps.gpindexer.fetcher.apk.ApkDownloader.APK_DOWNLOAD_MANAGER_PY

/**
 * Created by rurik on 01.02.15.
 */


class ApkDownloaderSpec extends Specification {
    String projectFolder = "/Users/rurik/IdeaProjects/GP_INDEXER"
    String packageName = "com.bakedspiderfree"
    Boolean isUpload = false
    Boolean isDelete = false

    ApkDownloader apkDownloader = new ApkDownloader();

    @IgnoreIf({ !(System.properties['os.name'].toString().toLowerCase().indexOf('mac') >= 0) })
    def "Apk is downloaded from GooglePlay successfuly"() {
        when:
        def filename = projectFolder + packageName + ".apk"


        def pyFile = projectFolder + "/" + APK_DOWNLOAD_MANAGER_PY
        apkDownloader.downloadApk(pyFile, packageName, projectFolder, isUpload, isDelete)

        then:
        File file = new File(filename);
        file.exists()

        cleanup:
        file.delete()
    }

}
