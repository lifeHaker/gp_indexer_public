package com.orangapps.gpindexer.fetcher.apk;

import com.orangapps.gpindexer.fetcher.helpers.parse.AppParser;
import com.orangapps.persistence.persist.sql.domain.Application;
import com.orangapps.persistence.persist.sql.enums.FileShares;
import com.orangapps.persistence.persist.sql.service.ApplicationService;
import com.orangapps.persistence.persist.sql.service.FileShareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.logging.Logger;

import static com.orangapps.gpindexer.common.utils.SleepUtility.ONE_SECOND;
import static com.orangapps.gpindexer.common.utils.SleepUtility.sleep;
import static com.orangapps.gpindexer.fetcher.helpers.parse.AppParsingUtility.getDecimalApkSize;

@Component
public class ApkDownloader {

    public static final String LOCAL_PATHNAME = "/Users/rurik/IdeaProjects/GP_INDEXER/";
    public static final String OPENSHIFT_DATA_DIR = "OPENSHIFT_DATA_DIR";
    public static final String VPS_DATA_DIR = "/home/test/";
    public static final String VPS_DATA_DIR_VAR = "VPS_DATA_DIR_VAR";

    public static final String GOOGLE_PLAY_API_FOLDER = "/home/test/";
    //    public static final String GOOGLE_PLAY_API_FOLDER = "/Users/rurik/IdeaProjects/GP_INDEXER/";
    public static final String APK_DOWNLOAD_MANAGER_PY = "googleplay-api-master/apkDownloadManager.py";
    public static final String PATH_TO_PYTHON_FILE = GOOGLE_PLAY_API_FOLDER + APK_DOWNLOAD_MANAGER_PY;
    public static final String SAVE_PATH = GOOGLE_PLAY_API_FOLDER;
    public static final double MIN_APK_SIZE = 1d;
    public Logger log = Logger.getLogger(ApkDownloader.class.getName());

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private FileShareService fileShareService;

    @Autowired
    AppParser appParser;

    public void startDownloadApks() {
        List<Application> appsToDownload = applicationService.getNotApkDownloadedApps(100);
        for (Application a : appsToDownload) {
            String packageName = a.getPackageName();
            double decimalApkSize = getDecimalApkSize(a.getSize());
            log.info("Apk " + packageName + " size is " + decimalApkSize);
            if (decimalApkSize > MIN_APK_SIZE) {
                downloadApk(PATH_TO_PYTHON_FILE, packageName, SAVE_PATH, true, true);
                fileShareService.createLink(a, FileShares.deposifiles);
                a.setIsApkDownloaded(true);
                a.updateExpirationDate();
                applicationService.update(a);
                log.info("Apk " + packageName + " is downloaded");
            }
            sleep(60 * ONE_SECOND);
        }
    }


    public String downloadApk(String pathToPythonFile, String mPackage, String savePath, Boolean isupload, Boolean isdelete) {
        try {
            ProcessBuilder pb = new ProcessBuilder("python", pathToPythonFile, mPackage, savePath, isupload.toString(), isdelete.toString());
            Process p = pb.start();
            BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuilder line = new StringBuilder();
            String s;
            while ((s = in.readLine()) != null) {
                line.append(s).append("  ");
            }
            BufferedReader error = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            while ((s = error.readLine()) != null) {
                line.append(s).append("  ");
            }
            log.info("py  answer " + line.toString());
            p.waitFor();
            return line.toString();
        } catch (Exception e) {
            log.severe("Error while downloading apk " + e.getMessage());
            return e.getLocalizedMessage();
        }
    }
}