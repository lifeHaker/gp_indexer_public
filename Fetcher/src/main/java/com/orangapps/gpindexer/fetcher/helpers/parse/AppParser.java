package com.orangapps.gpindexer.fetcher.helpers.parse;

import com.orangapps.persistence.persist.sql.domain.Application;
import com.orangapps.persistence.persist.sql.domain.Developer;
import com.orangapps.persistence.persist.sql.domain.Image;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.orangapps.gpindexer.fetcher.helpers.InfoPaths.*;

/**
 * Created by rurik on 21.11.15.
 */
@Component
public class AppParser {

    public static final String LOGO_JPG = "logo.jpg";
    public static final String SCREENSHOT = "screenshot";
    public static final String JPG = ".jpg";
    private final static String NONE_EMAIL = "NONE";
    public static final String APP_PACKAGE_CSS_PATH = "div[class=\"main-content\"]>div>div";

    public String getDeveloperEmail(Document document) {
        Elements devLinkElements = document.getElementsByClass("dev-link");
        for (Element item : devLinkElements) {
            if (item.attr("href").startsWith("mailto:")) {
                return item.attr("href").replaceAll("mailto:", "");
            }
        }
        return NONE_EMAIL;
    }

    public Developer getDeveloperInfo(Document document, boolean contentForSale) {
        Developer developer = new Developer();
        if (contentForSale) {
            developer.setName(getSellerWithContentForSale(document));
            developer.setEmail(getDeveloperEmail(document));
            developer.setAddress(getAddressWithContentForSale(document));
        } else {
            developer.setName(getSellerWithoutContentForSale(document));
            developer.setAddress(getDeveloperAddressWithoutContentForSale(document));
            developer.setEmail(getDeveloperEmail(document));
        }
        developer.setDevSite(getDeveloperSite(document));
        return developer;
    }

    public Application getApplicationInfo(Document document) {
        if (document == null) {
            return null;
        }
        Application application = new Application();
        application.setScore(getScore(document));
        application.setPackageName(getPackage(document));
        application.setLink(getLink(document));
        application.setName(getName(document));
        application.setCategory(getCategory(document));
        application.setDescription(getDescription(document));
        application.setScoreCount(getScoreCount(document));
        application.setLastUpdateDate(getLastUpdateDate(document));
        application.setInstallationCount(getInstallationCount(document));
        application.setSize(getSize(document));
        application.setSoftwareVersion(getSoftwareVersion(document));
        application.setAndroidVersion(getAndroidVersion(document));
        application.setContentRaiting(getContentRaiting(document));
        application.setSeller(getSellerWithContentForSale(document));
        application.setLogoImage(getLogoImage(document, LOGO_JPG));
        application.setVideoLink(getVideoLink(document));
        application.setScreenShots(getScreenShots(document, application));
        return application;
    }

    public String getPackageFromLink(Document document) {
        String url = getLink(document);
        return AppParsingUtility.getPackageFromUrl(url);
    }

    private String getPackage(Document document) {
        return document.select(APP_PACKAGE_CSS_PATH).attr(DATA_DOCID);
    }

    private String getDeveloperSite(Document document) {
        Elements devLinks = document.getElementsByClass("dev-link");
        for (Element item : devLinks) {
            if (!item.attr("href").startsWith("mailto:")) {
                return item.attr("href").replaceAll("mailto:", "");
            }
        }
        return "";
    }

    private String getDeveloperAddressWithoutContentForSale(Document document) {
        return getInfoByCssPath(document, getDeveloperAddressWithoutContentForSaleCssPath());
    }

    private String getAddressWithContentForSale(Document document) {
        return getInfoByCssPath(document, getDeveloperAddressWithContentForSaleCssPath());
    }

    private String getVideoLink(Document document) {
        Elements elements = document.select("span.play-action-container");
        if (elements.size() == 0) {
            return null;
        }
        Element videoLink = elements.get(0);
        return videoLink.attr("data-video-url");

    }

    private Image getLogoImage(Document document, String name) {
        Elements elements = document.select(getLogoImageCssPath());
        if (elements.size() == 0) {
            return null;
        }
        Element img = elements.get(0);
        String link = img.attr(SRC);
        return new Image(name, getPreparedLink(link));
    }

    private String getPreparedLink(String link) {
        int index = link.indexOf("=");
        return index > 0 ? link.substring(0, index) : link;
    }


    private String getSellerWithoutContentForSale(Document document) {
        return getInfoByCssPath(document, getSellerWithoutContentForSaleCssPath());
    }

    private String getContentForSale(Document document) {
        String contentForSale;
        if ((contentForSale = getInfoByCssPath(document, getContentForSaleCssPath())).length() == 0) {
            return null;
        }
        return contentForSale;
    }

    private String getSellerWithContentForSale(Document document) {
        return getInfoByCssPath(document, getSellerWithContentForSaleCssPath());
    }

    private String getContentRaiting(Document document) {
        return getInfoByCssPath(document, getContentRaitingCssPath());
    }

    private String getAndroidVersion(Document document) {
        return getInfoByCssPath(document, getAndroidVersionCssPath());
    }

    private String getSoftwareVersion(Document document) {
        return getInfoByCssPath(document, getSoftwareVersionCssPath());
    }

    private String getSize(Document document) {
        return getInfoByCssPath(document, getSizeCssPath());
    }

    private String getInstallationCount(Document document) {
        return getInfoByCssPath(document, getInstallationCountCssPath());
    }

    private String getLastUpdateDate(Document document) {
        return getInfoByCssPath(document, getUpdateDateCssPath());
    }

    private long getScoreCount(Document document) {
        try {
            return Long.parseLong(getInfoByCssPath(document, APPLICATION_SCORE_COUNT));
        } catch (Exception ex) {
            return 0;
        }
    }

    private double getScore(Document document) {
        try {
            return Double.parseDouble(getInfoByCssPath(document, APPLICATION_SCORE).replace(',', '.'));
        } catch (Exception ex) {
            return 0.0;
        }
    }

    private String getDescription(Document document) {
        return getInfoByCssPath(document, APPLICATION_DESCRIPTION);
    }

    private String getCategory(Document document) {
        return getInfoByCssPath(document, APPLICATION_CATEGORY);
    }

    private String getName(Document document) {
        return getInfoByCssPath(document, APPLICATION_NAME);
    }

    private String getLink(Document documnet) {
        return documnet.baseUri();
    }

    private List<Image> getScreenShots(Document document, Application application) {
        List<Image> screenShots = new ArrayList<>();
        Elements elements = document.select(getScreenShotsCssPath());
        int i = 0;
        for (Element e : elements) {
            screenShots.add(new Image(e.attr(SRC), application, getScreenshotName(i)));
            i++;
        }
        return screenShots;
    }

    private String getScreenshotName(int i) {
        return SCREENSHOT + i + JPG;
    }

    private String getInfoByCssPath(Document document, String cssPath) {
        return getParseDocument(document, cssPath).text().trim();
    }

    private Document getParseDocument(Document document, String cssPath) {
        return Jsoup.parse(document.select(cssPath).html());
    }

}
