package com.orangapps.gpindexer.fetcher.helpers;

import com.orangapps.gpindexer.fetcher.queryholder.AppsQueryHolder;
import com.orangapps.persistence.persist.sql.service.ApplicationService;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.logging.Logger;

import static com.orangapps.gpindexer.fetcher.helpers.InfoPaths.DATA_DOCID;
import static java.util.stream.Collectors.toList;

/**
 * Created by rurik on 18.02.2016.
 */
@Component
public class AppsFinder {
    private static final Logger log = Logger.getLogger(AppsFinder.class.getName());


    @Autowired
    @Qualifier(AppsQueryHolder.REDIS_APPS_QUERY_HOLDER)
    AppsQueryHolder appsQueryHolder;

    @Autowired
    private ApplicationService applicationService;

    public void findAppsAndAddThemToProcessing(Document page) {
        List<String> appsOnPage = findAppsOnPage(page);
        for (String id : appsOnPage) {
            if (isAppNotYetDownloaded(id) && !appsQueryHolder.isAdded(id) && !appsQueryHolder.isCompleted(id)) {
                appsQueryHolder.add(id);
                log.info("App id=" + id + " added to query.");
            }
        }
    }

    public List<String> findAppsOnPage(Document page) {
        Elements appsOnPage = page.getElementsByClass("card");
        return appsOnPage.stream().map(element -> element.attr(DATA_DOCID)).collect(toList());
    }

    public boolean isAppNotYetDownloaded(String appPackage) {
        return applicationService.getByPackage(appPackage) == null;
    }

}
