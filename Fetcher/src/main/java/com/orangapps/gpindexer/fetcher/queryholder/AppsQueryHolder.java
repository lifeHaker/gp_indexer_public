package com.orangapps.gpindexer.fetcher.queryholder;

/**
 * Created by rurik on 02.08.15.
 */
public interface AppsQueryHolder {

    String REDIS_APPS_QUERY_HOLDER = "RedisAppsQueryHolder";
    String STACK_APPS_QUERY_HOLDER = "StackAppsQueryHolder";

    void add(String app);

    Boolean isAdded(String app);

    String getNext();

    Boolean isCompleted(String app);

    void markAsCompleted(String app);

    Long getTaskPoolCount();

    Long getCompletedTasksCount();
}
