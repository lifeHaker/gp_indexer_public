package com.orangapps.gpindexer.fetcher.helpers;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created by rurik on 18.02.2016.
 */
@Component
public class GooglePlayPageLoader {
    private static final Logger log = Logger.getLogger(GooglePlayPageLoader.class.getName());

    public Document downloadPage(String url) {
        Document document = null;
        try {
            document = Jsoup.connect(url).get();
        } catch (IOException e) {
            log.severe("Connect to GooglePlay failed " + e.getMessage());
        }
        return document;
    }
}
