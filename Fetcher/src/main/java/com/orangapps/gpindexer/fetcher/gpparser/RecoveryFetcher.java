package com.orangapps.gpindexer.fetcher.gpparser;

import com.orangapps.gpindexer.fetcher.helpers.GooglePlayPageLoader;
import com.orangapps.gpindexer.fetcher.helpers.parse.AppParser;
import com.orangapps.gpindexer.fetcher.managment.SettingsManager;
import com.orangapps.persistence.persist.sql.domain.Application;
import com.orangapps.persistence.persist.sql.service.ApplicationService;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.logging.Logger;

import static com.orangapps.gpindexer.common.utils.SleepUtility.ONE_SECOND;
import static com.orangapps.gpindexer.common.utils.SleepUtility.sleep;
import static com.orangapps.gpindexer.fetcher.gpparser.AppsFetcher.GOOGLE_PLAY_PREFFIX;
import static com.orangapps.gpindexer.fetcher.gpparser.AppsFetcher.STORE_APPS_DETAILS_ID;

/**
 * Created by rurik on 28.08.2015.
 */

@Component
public class RecoveryFetcher {
    public Logger log = Logger.getLogger(RecoveryFetcher.class.getName());

    @Autowired
    private SettingsManager settingsManager;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    AppParser appParser;

    @Autowired
    GooglePlayPageLoader gpPageLoader;

    public void startRecoverApps() {
        log.info("isRecoveryActive=" + settingsManager.getIsGpFetchActive());
        if (settingsManager.isRecoveryActive()) {
            log.info("Recovery started");
            List<Application> appsForRecovery = applicationService.getAppsForRecovery(100);
            for (Application app : appsForRecovery) {
                sleep(10*ONE_SECOND);
                String packageName = app.getPackageName();
                Application downloadedApp = downloadApplicationInfo(packageName);
                Application dbApp = applicationService.getByPackage(packageName);
                if (dbApp != null && downloadedApp != null) {
                    dbApp.setName(downloadedApp.getName());
                    dbApp.setLink(downloadedApp.getLink());
                    applicationService.update(dbApp);
                }
            }
            log.info("Recovery stopped");
        }
    }

    public Application downloadApplicationInfo(String appPackage) {//TODO test it
        Document document = gpPageLoader.downloadPage(GOOGLE_PLAY_PREFFIX + STORE_APPS_DETAILS_ID + appPackage);
        return appParser.getApplicationInfo(document);
    }

}
