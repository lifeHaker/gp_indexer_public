package com.orangapps.gpindexer.fetcher.gpparser;

import com.orangapps.gpindexer.common.utils.SleepUtility;
import com.orangapps.gpindexer.fetcher.helpers.AppsFinder;
import com.orangapps.gpindexer.fetcher.helpers.GooglePlayPageLoader;
import com.orangapps.gpindexer.fetcher.helpers.parse.AppParser;
import com.orangapps.gpindexer.fetcher.queryholder.AppsQueryHolder;
import com.orangapps.persistence.persist.nosql.mongo.service.ApplicationMongoService;
import com.orangapps.persistence.persist.sql.domain.Application;
import com.orangapps.persistence.persist.sql.domain.Developer;
import com.orangapps.persistence.persist.sql.service.ApplicationService;
import com.orangapps.persistence.persist.sql.service.DeveloperService;
import com.orangapps.persistence.utils.NumberUtils;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

import static com.orangapps.gpindexer.common.utils.SleepUtility.ONE_SECOND;

@Component
public class AppsFetcher extends Thread {
    public static final String STORE_APPS_DETAILS_ID = "/store/apps/details?hl=ru&id=";

    public static final String GOOGLE_PLAY_PREFFIX = "https://play.google.com";
    private static final Logger log = Logger.getLogger(AppsFetcher.class.getName());
    public static final int MIN_INSTALLATION_COUNT = 10;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private ApplicationMongoService applicationMongoService;

    @Autowired
    private DeveloperService developerService;

    @Autowired
    @Qualifier(AppsQueryHolder.REDIS_APPS_QUERY_HOLDER)
    AppsQueryHolder appsQueryHolder;

    @Autowired
    AppParser appParser;

    @Autowired
    GooglePlayPageLoader gpPageLoader;

    @Autowired
    AppsFinder appsFinder;

    public AppsFetcher() {
    }

    public void startParseAppPages() {
        String currentApp;
        while ((currentApp = appsQueryHolder.getNext()) != null) {
            log.info("Start fetching app id=" + currentApp);
            String url = GOOGLE_PLAY_PREFFIX + STORE_APPS_DETAILS_ID + currentApp;
            Document page = gpPageLoader.downloadPage(url);
            if (page != null) {
                appsFinder.findAppsAndAddThemToProcessing(page);
                if (appsFinder.isAppNotYetDownloaded(currentApp)) {
                    parsePageAndSaveApp(page);
                }
            }
            SleepUtility.sleep(30 * ONE_SECOND);
            log.info("Stop fetching app id=" + currentApp);
        }
    }

    private void parsePageAndSaveApp(Document appPage) {
        Application application = appParser.getApplicationInfo(appPage);
        if (application != null) {
            int count = NumberUtils.getIntCount(application.getInstallationCount());
            String packageName = application.getPackageName();
            log.info("Parsing app " + packageName + " installation count is " + count + " min=" + MIN_INSTALLATION_COUNT);
            if (count >= MIN_INSTALLATION_COUNT) {
                Developer developer = createDeveloperIfNotExist(appPage, application.getContentForSale() != null);
                applicationService.create(application);
                application.setDeveloper(developer);
                applicationService.update(application);
                applicationMongoService.save(application);
                appsQueryHolder.markAsCompleted(packageName);
                log.info("App " + packageName + " downloaded and marked as completed");
            }
        }
    }

    private Developer createDeveloperIfNotExist(Document appPage, boolean isContentForSale) {
        String email = appParser.getDeveloperEmail(appPage);
        Developer developer = developerService.findByEmail(email);
        if (developer == null) {
            developer = appParser.getDeveloperInfo(appPage, isContentForSale);
            log.info("Saving developer=" + developer.getName());
            developerService.create(developer);
            developer = developerService.findByEmail(email);
        }
        return developer;
    }
}
