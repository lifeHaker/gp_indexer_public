package com.orangapps.gpindexer.fetcher.queryholder.stack;

import com.orangapps.gpindexer.fetcher.queryholder.AppsQueryHolder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.EmptyStackException;
import java.util.HashSet;
import java.util.Stack;

/**
 * Created by rurik on 19.07.15.
 */
@Component
@Qualifier(AppsQueryHolder.STACK_APPS_QUERY_HOLDER)
public class StackAppsQueryHolder implements AppsQueryHolder {

    private Stack<String> taskPool;

    private HashSet<String> completeTasks;

    public StackAppsQueryHolder() {
        taskPool = new Stack<>();
        completeTasks = new HashSet<>();
    }


    @Override
    public void add(String app) {
        taskPool.add(app);
    }

    @Override
    public Boolean isAdded(String app) {
        return taskPool.contains(app);
    }

    @Override
    public String getNext() {
        try {
            return taskPool.pop();
        } catch (EmptyStackException e) {
            return null;
        }
    }

    @Override
    public Boolean isCompleted(String app) {
        return completeTasks.contains(app);
    }

    @Override
    public void markAsCompleted(String app) {
        completeTasks.add(app);
    }

    @Override
    public Long getTaskPoolCount() {
        return Long.valueOf(taskPool.size());
    }

    @Override
    public Long getCompletedTasksCount() {
        return Long.valueOf(completeTasks.size());
    }
}
