package com.orangapps.gpindexer.fetcher.queryholder.redis;

import com.orangapps.gpindexer.fetcher.queryholder.AppsQueryHolder;
import com.orangapps.gpindexer.common.redis.set.RedisSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by rurik on 19.07.15.
 */
@Component
@Qualifier(AppsQueryHolder.REDIS_APPS_QUERY_HOLDER)
public class RedisAppsQueryHolder implements AppsQueryHolder {

    @Autowired
    @Qualifier(RedisSet.TASK_POOL_SET)
    private RedisSet taskPool;

    @Autowired
    @Qualifier(RedisSet.COMPLETED_TASKS_SET)
    private RedisSet completeTasks;

    @Override
    public void add(String app) {
        taskPool.add(app);
    }

    @Override
    public Boolean isAdded(String app) {
        return taskPool.exist(app);
    }

    @Override
    public String getNext() {
        return taskPool.pop();
    }

    @Override
    public Boolean isCompleted(String app) {
        return completeTasks.exist(app);
    }

    @Override
    public void markAsCompleted(String app) {
        completeTasks.add(app);
    }

    @Override
    public Long getTaskPoolCount() {
        return taskPool.getCount();
    }

    @Override
    public Long getCompletedTasksCount() {
        return completeTasks.getCount();
    }


}
