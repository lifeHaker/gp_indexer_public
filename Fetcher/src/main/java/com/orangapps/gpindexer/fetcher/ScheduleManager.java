package com.orangapps.gpindexer.fetcher;

import com.orangapps.gpindexer.fetcher.apk.ApkDownloader;
import com.orangapps.gpindexer.fetcher.gpparser.AppPackagesFetcher;
import com.orangapps.gpindexer.fetcher.gpparser.AppsFetcher;
import com.orangapps.gpindexer.fetcher.managment.SettingsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

/**
 * Created by rurik on 18.02.2016.
 */

@Component
public class ScheduleManager {
    private static final Logger logger = Logger.getLogger(ScheduleManager.class.getName());

    public static final int MIN = 60 * 1000;

    @Autowired
    AppsFetcher appsFetcher;

    @Autowired
    AppPackagesFetcher appPackagesFetcher;

    @Autowired
    ApkDownloader apkDownloader;

    @Autowired
    private SettingsManager settingsManager;

    @Scheduled(fixedDelay = 90 * MIN)
    public void parseAppPages() {
        logger.info("IsGpDownloadActive=" + settingsManager.getIsGpDownloadActive());
        if (settingsManager.getIsGpDownloadActive()) {
            appsFetcher.startParseAppPages();
        }
    }

    @Scheduled(fixedDelay = 70 * MIN)
    public void fetchNewAppIds() {
        logger.info("isGpFetchActive=" + settingsManager.getIsGpFetchActive());
        if (settingsManager.getIsGpFetchActive()) {
            appPackagesFetcher.startFetchNewAppIds();
        }
    }

    @Scheduled(fixedDelay = 50 * MIN)
    public void downloadApks() {
        logger.info("IsApkDownloadActive=" + settingsManager.getIsApkDownloadActive());
        if (settingsManager.getIsApkDownloadActive()) {
            apkDownloader.startDownloadApks();
        }
    }

}
