package com.orangapps.gpindexer.fetcher.gpparser;

import com.orangapps.gpindexer.common.utils.SleepUtility;
import com.orangapps.gpindexer.fetcher.helpers.AppsFinder;
import com.orangapps.gpindexer.fetcher.helpers.GooglePlayPageLoader;
import com.orangapps.gpindexer.fetcher.queryholder.AppsQueryHolder;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by rurik on 19.07.15.
 */

@Service
public class AppPackagesFetcher {
    private static final Logger log = Logger.getLogger(AppPackagesFetcher.class.getName());

    public static final String searchAppUrl = "https://play.google.com/store/search?hl=en&q=%s&c=apps";
    public static final int MIN_TASK_COUNT = 1000;
    public static List wordsToSearch = Arrays.asList("abcdefghijklmnopqrstuvwxyz".toCharArray());

    public static List<String> urlToSearch = Arrays.asList(
            "https://play.google.com/store/apps/new/category/GAME?hl=ru",
            "https://play.google.com/store/apps/top/category/GAME?hl=ru",
            "https://play.google.com/store/apps?hl=ru",
            "https://play.google.com/store/apps/top?hl=ru",
            "https://play.google.com/store/apps/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/GAME/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/BUSINESS/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/APP_WIDGETS/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/APP_WALLPAPER/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/HEALTH_AND_FITNESS/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/TOOLS/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/BOOKS_AND_REFERENCE/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/COMICS/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/MEDICAL/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/MUSIC_AND_AUDIO/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/MEDIA_AND_VIDEO/collection/topselling_free?hl=ru",

            "https://play.google.com/store/apps/category/NEWS_AND_MAGAZINES/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/EDUCATION/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/PERSONALIZATION/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/WEATHER/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/SHOPPING/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/TRAVEL_AND_LOCAL/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/PRODUCTIVITY/collection/topselling_free?hl=ru",

            "https://play.google.com/store/apps/category/GAME_ARCADE/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/GAME_TRIVIA/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/GAME_RACING/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/GAME_CASINO/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/GAME_CASUAL/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/GAME_CARD/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/GAME_MUSIC/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/GAME_BOARD/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/GAME_EDUCATIONAL/collection/topselling_free?hl=ru",

            "https://play.google.com/store/apps/category/GAME_PUZZLE/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/GAME_ADVENTURE/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/GAME_ROLE_PLAYING/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/GAME_WORD/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/GAME_SPORTS/collection/topselling_free?hl=ru",
            "https://play.google.com/store/apps/category/GAME_STRATEGY/collection/topselling_free?hl=ru"
    );

    @Autowired
    @Qualifier(AppsQueryHolder.REDIS_APPS_QUERY_HOLDER)
    AppsQueryHolder appsQueryHolder;

    @Autowired
    GooglePlayPageLoader gpPageLoader;

    @Autowired
    AppsFinder appsFinder;

    public void startFetchNewAppIds() {
        if (appsQueryHolder.getTaskPoolCount() < MIN_TASK_COUNT) {
            findNewApps();
        }
    }

    public void findNewApps() {
        log.info("Start finding new apps");
        for (String url : urlToSearch) {
            parseUrl(url);
        }
        for (Object w : wordsToSearch) {
            SleepUtility.sleep(100);
            String word = String.valueOf(w);
            String url = String.format(searchAppUrl, word);
            parseUrl(url);
        }

        for (Object w : wordsToSearch) {
            for (Object ww : wordsToSearch) {
                SleepUtility.sleep(100);
                String word = String.valueOf(w) + String.valueOf(ww);
                String url = String.format(searchAppUrl, word);
                parseUrl(url);
            }
        }
        log.info("Stop finding new apps");
    }

    private void parseUrl(String url) {
        Document page = gpPageLoader.downloadPage(url);
        if (page != null) {
            appsFinder.findAppsAndAddThemToProcessing(page);
        }
    }
}
