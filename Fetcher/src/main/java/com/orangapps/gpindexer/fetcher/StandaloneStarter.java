package com.orangapps.gpindexer.fetcher;

import com.orangapps.gpindexer.fetcher.gpparser.AppPackagesFetcher;
import org.springframework.context.support.GenericGroovyApplicationContext;
import org.springframework.stereotype.Component;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;

/**
 *
 */
@Component
public class StandaloneStarter {

    public static void main(String[] args) throws MalformedObjectNameException, NotCompliantMBeanException, InstanceAlreadyExistsException, MBeanRegistrationException {
        GenericGroovyApplicationContext ctx = new GenericGroovyApplicationContext("classpath:fetcher-beans.groovy");

        AppPackagesFetcher appPackagesFetcher = ctx.getBean(AppPackagesFetcher.class);
        appPackagesFetcher.startFetchNewAppIds();
    }
}
