package com.orangapps.gpindexer.fetcher.helpers.parse;

import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by rurik on 19.02.16.
 */
public class AppParsingUtility {
    private static final Logger log = Logger.getLogger(AppParsingUtility.class.getName());
    public static final String MB = "M";
    public static final String KB = "k";

    public static String getPackageFromUrl(String url) {
        return url.substring(url.indexOf("id=") + 3);
    }

    public static double getDecimalApkSize(String strSize) {
        try {
            String patternMb = "(\\d+.?\\d*)\\D?";

            Pattern r = Pattern.compile(patternMb);
            Matcher m = r.matcher(strSize);

            String str = "0";
            if (m.find()) {
                str = m.group(0);
                str = str.trim();
            }
            int length = str.length();
            String decimalStr = str.substring(0, length - 1);
            decimalStr = decimalStr.replace(",", ".");
            if (str.endsWith(MB)) {
                return Double.parseDouble(decimalStr);
            } else if (str.endsWith(KB)) {
                return Double.parseDouble("0." + decimalStr);
            } else if (!strSize.isEmpty()) {// for "Varies with device"
                return Integer.MAX_VALUE;
            }
            return 0;
        } catch (Exception e) {
            log.severe("Can't parse: " + strSize + " error is: " + e.getMessage());
        }
        return 0;
    }
}
