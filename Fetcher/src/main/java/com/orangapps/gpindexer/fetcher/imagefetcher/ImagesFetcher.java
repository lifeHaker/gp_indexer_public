package com.orangapps.gpindexer.fetcher.imagefetcher;

import com.orangapps.gpindexer.common.up_down_load_utils.ImageDownloader;
import com.orangapps.gpindexer.common.up_down_load_utils.YandexApiRunner;
import com.orangapps.gpindexer.fetcher.managment.SettingsManager;
import com.orangapps.persistence.persist.sql.domain.Application;
import com.orangapps.persistence.persist.sql.domain.Image;
import com.orangapps.persistence.persist.sql.service.ApplicationService;
import com.yandex.disk.rest.exceptions.ServerIOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import static com.orangapps.gpindexer.common.utils.SleepUtility.sleep;

/**
 * Created by rurik on 30.07.15.
 */


@Component
public class ImagesFetcher {

    private static final Logger log = Logger.getLogger(ImagesFetcher.class.getName());

    @Autowired
    private ImageDownloader imageDownloader;

    @Autowired
    private YandexApiRunner yandexApiRunner;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private SettingsManager settingsManager;

    public void startDownloadImages() {
        if (settingsManager.getIsImagesDownloadActive()) {
            List<Application> appsToDownload = applicationService.getNotImagesDownloadedApps(100);
            for (Application a : appsToDownload) {
                log.info("------start download image  for " + a.getPackageName());
                try {
                    yandexApiRunner.createAppFolder(a);
                    log.info("------created folder for " + a.getPackageName());
                } catch (IOException | ServerIOException e) {
                    log.severe(e.getStackTrace().toString());
                }
                List<Image> images = a.getScreenShots();
                images.add(a.getLogoImage());
                for (Image i : images) {
                    try {
                        uploadImage(i, a.getPackageName());
                    } catch (IOException e) {
                        log.severe(e.getStackTrace().toString());
                    }
                    sleep(3000);
                }
                a.setIsImagesDownloaded(true);
                applicationService.update(a);
                log.info("-------end download image for" + a.getPackageName());
            }
        }
    }

    private void uploadImage(Image i, String path) throws IOException {
        String nameToSave = i.getName();
        String tempFile = imageDownloader.downloadImageAndSaveToTempFile(i.getGooglePlayLink(), nameToSave);
        yandexApiRunner.uploadFile(tempFile, path);
    }


}
