package com.orangapps.gpindexer.fetcher.managment;

import org.springframework.stereotype.Component;

/**
 * Created by rurik on 07.08.2015.
 */

@Component
public class SettingsManager {

    private volatile Boolean isGpFetchActive = false;
    private volatile Boolean isGpDownloadActive = false;
    private volatile Boolean isApkDownloadActive = false;
    private volatile Boolean isImagesDownloadActive = false;
    private volatile Boolean isRecoveryActive = false;

    private volatile Integer appsToDownloadSize = 100;

    public Integer getAppsToDownloadSize() {
        return appsToDownloadSize;
    }

    public void setAppsToDownloadSize(Integer appsToDownloadSize) {
        this.appsToDownloadSize = appsToDownloadSize;
    }

    public Boolean getIsImagesDownloadActive() {
        return isImagesDownloadActive;
    }

    public void setIsImagesDownloadActive(Boolean isImagesDownloadActive) {
        this.isImagesDownloadActive = isImagesDownloadActive;
    }

    public Boolean getIsApkDownloadActive() {
        return isApkDownloadActive;
    }

    public void setIsApkDownloadActive(Boolean isApkDownloadActive) {
        this.isApkDownloadActive = isApkDownloadActive;
    }

    public Boolean getIsGpFetchActive() {
        return isGpFetchActive;
    }

    public void setIsGpFetchActive(Boolean isGpFetchActive) {
        this.isGpFetchActive = isGpFetchActive;
    }

    public Boolean getIsGpDownloadActive() {
        return isGpDownloadActive;
    }

    public void setIsGpDownloadActive(Boolean isGpDownloadActive) {
        this.isGpDownloadActive = isGpDownloadActive;
    }


    public Boolean isRecoveryActive() {
        return isRecoveryActive;
    }

    public void setIsRecoveryActive(Boolean isRecoveryActive) {
        this.isRecoveryActive = isRecoveryActive;
    }
}
