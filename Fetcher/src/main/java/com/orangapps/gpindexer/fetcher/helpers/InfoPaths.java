package com.orangapps.gpindexer.fetcher.helpers;

import static java.lang.String.*;

/**
 *
 */
public class InfoPaths {
    public static final String COMMON_PATH_TEMPLATE = "%s[itemprop=\"%s\"]";
    public static final String DIV = "div";
    public static final String SPAN = "span";
    public static final String H1 = "h1";
    public static final String DATA_DOCID = "data-docid";

    public static final String APPLICATION_NAME = format(COMMON_PATH_TEMPLATE, H1, "name");
    public static final String APPLICATION_CATEGORY = format(COMMON_PATH_TEMPLATE, SPAN, "genre");
    public static final String APPLICATION_DESCRIPTION = format(COMMON_PATH_TEMPLATE, DIV, "description");
    public static final String APPLICATION_SCORE = format(COMMON_PATH_TEMPLATE, DIV, "aggregateRating") + "> div.score";
    public static final String APPLICATION_SCORE_COUNT = format(COMMON_PATH_TEMPLATE, DIV, "aggregateRating") + "> div.reviews-stats>span";
    public static final String IMG = "img";
    public static final String SRC = "src";

    //Details zone
    public static String getUpdateDateCssPath() {
        return format(COMMON_PATH_TEMPLATE, DIV, "datePublished");
    }

    public static String getInstallationCountCssPath() {
        return format(COMMON_PATH_TEMPLATE, DIV, "numDownloads");
    }

    public static String getSizeCssPath() {
        return format(COMMON_PATH_TEMPLATE, DIV, "fileSize");
    }

    public static String getSoftwareVersionCssPath() {
        return format(COMMON_PATH_TEMPLATE, DIV, "softwareVersion");
    }

    public static String getAndroidVersionCssPath() {
        return format(COMMON_PATH_TEMPLATE, DIV, "operatingSystems");
    }

    public static String getContentRaitingCssPath() {
        return format(COMMON_PATH_TEMPLATE, DIV, "contentRating");
    }

    public static String getSellerWithContentForSaleCssPath() {
        return getSellerCssPath();
    }

    public static String getSellerWithoutContentForSaleCssPath() {
        return getSellerCssPath();
    }

    public static String getContentForSaleCssPath() {
        return format(COMMON_PATH_TEMPLATE, DIV, "");
    }

    public static String getDeveloperAddressWithContentForSaleCssPath() {
        return format(COMMON_PATH_TEMPLATE, DIV, "");
    }

    public static String getDeveloperAddressWithoutContentForSaleCssPath() {
        return format(COMMON_PATH_TEMPLATE, DIV, "");
    }

    public static String getDeveloperSiteCssPath(boolean contentForSale) {
        return format(COMMON_PATH_TEMPLATE, DIV, "");

    }


    public static String getSellerCssPath() {
        return format(COMMON_PATH_TEMPLATE, DIV, "author") + "> a.primary > span";
    }

    public static String getDeveloperAddressCssPath() {
        return format(COMMON_PATH_TEMPLATE, DIV, "");
    }

    public static String getDeveloperEmailCssPath(boolean contentForSale) {
        return "a.dev-link[href^=mailto:]";
    }

    public static String getLogoImageCssPath() {
        return format(COMMON_PATH_TEMPLATE, IMG, "image");
    }

    public static String getScreenShotsCssPath() {
        return format(COMMON_PATH_TEMPLATE, IMG, "screenshot");
    }

}
