package com.orangapps.gpindexer.common.utils;

import java.io.File;

/**
 * Created by rurik on 01.08.15.
 */
public class FileUtils {

    public static void deleteFileOnExit(File file) {
        file.deleteOnExit();
    }

}
