package com.orangapps.gpindexer.common.redis;

/**
 *
 */
public class RedisKeyNames {
    public static final String TASKS_POOL_KEY = "tasksPool";
    public static final String COMPLETE_TASKS_KEY = "completeTask";
    public static final String EMAILS_POOL_KEY = "emailsPool";
    public static final String USED_EMAILS = "usedEmailPool";
}
