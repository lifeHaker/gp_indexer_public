package com.orangapps.gpindexer.common.redis.set;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by rurik on 02.08.15.
 */

@Component
@Qualifier(RedisSet.TASK_POOL_SET)
public class TaskPoolSet extends RedisSet {
    public TaskPoolSet() {

        super(TaskPoolSet.class.getName());
    }
}
