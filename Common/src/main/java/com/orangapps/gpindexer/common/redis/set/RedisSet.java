package com.orangapps.gpindexer.common.redis.set;

import com.orangapps.gpindexer.common.redis.AbstractRedisModel;
import org.springframework.beans.factory.annotation.Autowired;
import redis.clients.jedis.Jedis;

import javax.annotation.PostConstruct;

/**
 *
 */
//@Service
public class RedisSet extends AbstractRedisModel {
    public static final String TASK_POOL_SET = "TaskPoolSet";
    public static final String COMPLETED_TASKS_SET = "CompletedTasksSet";

    @Autowired
    private Jedis client;

    public RedisSet() {
        super();
    }

    public RedisSet(String name) {
        super(name);

    }


    @PostConstruct
    public void auth() {
    }

    public String pop() {
        return client.spop(key);
    }

    public void add(String... value) {
        client.sadd(key, value);
    }

    public boolean exist(String value) {
        return client.sismember(key, value);
    }

    public  Long getCount(){
        return client.scard(getKey());
    }

}
