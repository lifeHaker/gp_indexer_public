package com.orangapps.gpindexer.common.up_down_load_utils;

import org.springframework.stereotype.Component;

import java.io.*;
import java.net.URL;

/**
 * Created by rurik on 01.08.15.
 */
@Component
public class ImageDownloader {

    public String downloadImageAndSaveToTempFile(String fromUrl, String nameToSave) throws IOException {
        URL url = new URL(fromUrl);
        InputStream in = new BufferedInputStream(url.openStream());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        int n = 0;
        while (-1 != (n = in.read(buf))) {
            out.write(buf, 0, n);
        }
        out.close();
        in.close();
        byte[] response = out.toByteArray();
        FileOutputStream fos = new FileOutputStream(nameToSave);
        fos.write(response);
        fos.close();
        return nameToSave;
    }
}
