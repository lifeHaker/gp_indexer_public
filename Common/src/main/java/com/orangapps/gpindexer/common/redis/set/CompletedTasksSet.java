package com.orangapps.gpindexer.common.redis.set;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by rurik on 02.08.15.
 */
@Component
@Qualifier(RedisSet.COMPLETED_TASKS_SET)
public class CompletedTasksSet extends RedisSet {

    public CompletedTasksSet() {
        super(CompletedTasksSet.class.getName());
    }
}
