package com.orangapps.gpindexer.common.up_down_load_utils;

import com.orangapps.persistence.persist.sql.domain.Application;
import com.orangapps.gpindexer.common.utils.FileUtils;
import com.yandex.disk.rest.Credentials;
import com.yandex.disk.rest.RestClient;
import com.yandex.disk.rest.exceptions.ServerException;
import com.yandex.disk.rest.exceptions.ServerIOException;
import com.yandex.disk.rest.json.Link;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

/**
 * Created by rurik on 28.07.2015.
 */

@Component
public class YandexApiRunner {
    public static final String URL = "https://oauth.yandex.ru%2Fauthorize%3Fresponse_type%3Dtoken%26client_id%3D9e22b4cc5042458796e9eca1c481d7e3";

//    public static final String AUTH_URL = "https://oauth.yandex.ru/authorize?response_type=token&client_id=9e22b4cc5042458796e9eca1c481d7e3";
//    public static final String CLIENT_ID = "9e22b4cc5042458796e9eca1c481d7e3";
//    public static final String CLIENT_SECRET = "eeab3e50f5d94501988060b505bc4e10";
//    public static final String TOKEN = "a73f925b543d4af693c122721519e5ef";
//    public static final String USER = "urrast";


    public static final String AUTH_URL = "https://oauth.yandex.ru/authorize?response_type=token&client_id=ac890f6e0fdc41ceb53eedd32ba0f968";
    public static final String CLIENT_ID = "test";
    public static final String CLIENT_SECRET = "test";
    public static final String TOKEN = "test";
    public static final String USER = "test";

    public static final String SERVER_FOLDER = "/GP_FETCHER";
    public static final String DELIMITER = "/";

    public void uploadFile(String localFileName, String path) {
        RestClient restClient = new RestClient(new Credentials(USER, TOKEN));
        try {
            Link link = restClient.getUploadLink(SERVER_FOLDER + DELIMITER + path + DELIMITER + localFileName, true);
            File file = new File(localFileName);
            restClient.uploadFile(link, true, file, null);
            FileUtils.deleteFileOnExit(file);
        } catch (IOException | ServerException ex) {
            ex.printStackTrace();
        }

    }


    public void downloadFile(String serverPath, String localFile) {
        RestClient restClient = new RestClient(new Credentials(USER, TOKEN));
        try {
            restClient.downloadFile(serverPath, new File(localFile), null);
        } catch (IOException | ServerException ex) {
            ex.printStackTrace();
        }
    }

    public void createAppFolder(Application application) throws IOException, ServerIOException {
        RestClient restClient = new RestClient(new Credentials(USER, TOKEN));
        restClient.makeFolder(SERVER_FOLDER + DELIMITER + application.getPackageName());
    }


}
