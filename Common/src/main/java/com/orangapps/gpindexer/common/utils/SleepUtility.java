package com.orangapps.gpindexer.common.utils;

import java.util.logging.Logger;

/**
 * Created by rurik on 17.02.16.
 */
public class SleepUtility {
    public static final int ONE_SECOND = 1000;
    public static Logger log = Logger.getLogger(SleepUtility.class.getName());

    public static void sleep(int millis) {
        try {
            Thread.currentThread().sleep(millis);
        } catch (InterruptedException e) {
            log.severe("sleep failed " + e.getMessage());
        }
    }
}
