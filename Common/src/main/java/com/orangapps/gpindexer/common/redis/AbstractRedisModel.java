package com.orangapps.gpindexer.common.redis;

/**
 *
 */
public abstract class AbstractRedisModel {
    protected String key;

    public AbstractRedisModel(String key) {
        this.key = key;
    }

    public AbstractRedisModel() {

    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
