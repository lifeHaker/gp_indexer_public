import org.apache.commons.dbcp.BasicDataSource
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import redis.clients.jedis.Jedis

beans {
    xmlns context: "http://www.springframework.org/schema/context"
    xmlns tx: "http://www.springframework.org/schema/tx"
    tx.'annotation-driven'()

    def openshift_mode = environment.activeProfiles.contains("openshift_profile")


    localDataSource(BasicDataSource) {
        driverClassName = "org.postgresql.Driver"
        url = "jdbc:postgresql://localhost:5432/TEST"
        username = 'TEST'
        password = 'TEST'
    }

    openshiftDataSource(BasicDataSource) {
        driverClassName = "org.postgresql.Driver"
        url = "jdbc:postgresql://172.16.5.251:42821/TEST"
        username = 'TEST'
        password = 'TEST'
    }

    entityManagerFactory(LocalContainerEntityManagerFactoryBean) {
        persistenceProviderClass = "org.hibernate.ejb.HibernatePersistence"
        if (openshift_mode) {
            dataSource = openshiftDataSource
        } else {
            dataSource = localDataSource
        }
        persistenceUnitName = "persistenceUnitDefault"
        packagesToScan = ["com.orangapps.persistence.persist.sql.domain"]
        jpaProperties = [
                "hibernate.format_sql"  : "false",
                "hibernate.show_sql"    : "false",
                "hibernate.hbm2ddl.auto": "update",
                "hibernate.dialect"     : "org.hibernate.dialect.PostgreSQL9Dialect"
        ]
    }

    transactionManager(JpaTransactionManager) {
        entityManagerFactory = entityManagerFactory
    }

    if (openshift_mode) {
        redisClient(Jedis, "172.16.8.36", "55701")
    } else {
        redisClient(Jedis, 'localhost')
    }

}