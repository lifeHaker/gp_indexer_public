package com.orangapps.gpindexer.web.controller;

import com.orangapps.gpindexer.fetcher.queryholder.AppsQueryHolder;
import com.orangapps.gpindexer.fetcher.managment.SettingsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.logging.Logger;

/**
 * Created by rurik on 01.03.15.
 */
@Controller
public class StatisticsController {
    private static final Logger log = Logger.getLogger(StatisticsController.class.getName());
    public static final int TRY_COUNT = 5;

    @Autowired
    @Qualifier(AppsQueryHolder.REDIS_APPS_QUERY_HOLDER)
    AppsQueryHolder appsQueryHolder;

    @Autowired
    private SettingsManager settingsManager;

    @RequestMapping("/get_statistics")
    @ResponseBody
    public String getStatistics() throws InterruptedException {
        for (int i = 0; i < TRY_COUNT; i++) {
            try {
                Long taskPoolCount = appsQueryHolder.getTaskPoolCount();
                Long completedTasksCount = appsQueryHolder.getCompletedTasksCount();
                return String.format("Tasks to process: %s \n, Completed tasks: %s", taskPoolCount, completedTasksCount);
            } catch (Exception e) {
                log.severe("Failed to get statistics "+e.getMessage());
            }
        }
        return "Get statistics failed after "+TRY_COUNT+" attempts";
    }

}
