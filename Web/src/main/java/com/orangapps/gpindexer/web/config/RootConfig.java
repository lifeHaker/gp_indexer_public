package com.orangapps.gpindexer.web.config;

import org.springframework.context.annotation.*;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by rurik on 08.03.15.
 */
@Configuration
@ComponentScan("com.orangapps")
@EnableScheduling
@ImportResource("classpath:/root-config.groovy")
@Import({WebConfig.class, SecurityConfig.class})
public class RootConfig {

    @Bean(destroyMethod = "shutdown")
    public Executor taskScheduler() {
        return Executors.newScheduledThreadPool(4);
    }

}
