package com.orangapps.gpindexer.web.controller;

import com.orangapps.gpindexer.fetcher.managment.SettingsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;
import java.util.logging.Logger;

/**
 * Created by rurik on 01.03.15.
 */
@Controller
public class ManagementController implements ServletContextAware {

    private static final Logger log = Logger.getLogger(ManagementController.class.getName());

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private SettingsManager settingsManager;

    @RequestMapping("/start_gp_indexing")
    @ResponseBody
    public void run() {
        settingsManager.setIsApkDownloadActive(true);
        settingsManager.setIsGpDownloadActive(true);
        settingsManager.setIsGpFetchActive(true);
        settingsManager.setIsRecoveryActive(true);

        log.info("==============start_gp_indexing======");
    }

    @RequestMapping("/stop_apk_download")
    @ResponseBody
    public void stopApkDownload() {
        settingsManager.setIsApkDownloadActive(false);
    }

    @RequestMapping("/start_apk_download")
    @ResponseBody
    public void startApkDownload() {
        settingsManager.setIsApkDownloadActive(true);
    }

    @RequestMapping("/start_gp_fetch")
    @ResponseBody
    public void startGpFetch() {
        settingsManager.setIsGpFetchActive(true);
    }

    @RequestMapping("/stop_gp_fetch")
    @ResponseBody
    public void stopGpFetch() {
        settingsManager.setIsGpFetchActive(false);
    }

    @RequestMapping("/stop_images_download")
    @ResponseBody
    public void stopImagesDownload() {
        settingsManager.setIsImagesDownloadActive(false);
    }

    @RequestMapping("/start_images_download")
    @ResponseBody
    public void startImagesDownload() {
        settingsManager.setIsImagesDownloadActive(true);
    }

    @RequestMapping("/start_gp_download")
    @ResponseBody
    public void startGpDownload() {
        settingsManager.setIsGpDownloadActive(true);
    }

    @RequestMapping("/start_recovery")
    @ResponseBody
    public void startRecovery() {
        settingsManager.setIsRecoveryActive(true);
    }

    @RequestMapping("/stop_recovery")
    @ResponseBody
    public void stopRecovery() {
        settingsManager.setIsRecoveryActive(false);
    }

    @RequestMapping("/start_gp_fetch_and_download")
    @ResponseBody
    public void startGpFetchAndDownload() {
        settingsManager.setIsGpDownloadActive(true);
        settingsManager.setIsGpFetchActive(true);
    }

    @RequestMapping("/stop_gp_fetch_and_download")
    @ResponseBody
    public void stopGpFetchAndDownload() {
        settingsManager.setIsGpDownloadActive(false);
        settingsManager.setIsGpFetchActive(false);
    }



    @RequestMapping("/stop_gp_download")
    @ResponseBody
    public void stopGpDownload() {
        settingsManager.setIsGpDownloadActive(false);
    }

    @RequestMapping("/stop_all")
    @ResponseBody
    public void stopAll() {
        settingsManager.setIsApkDownloadActive(false);
        settingsManager.setIsGpDownloadActive(false);
        settingsManager.setIsGpFetchActive(false);
        settingsManager.setIsRecoveryActive(false);
        settingsManager.setIsImagesDownloadActive(false);
    }

    @Override
    public void setServletContext(ServletContext sServletContext) {
        servletContext = sServletContext;
    }


    public void setSettingsManager(SettingsManager settingsManager) {
        this.settingsManager = settingsManager;
    }
}
