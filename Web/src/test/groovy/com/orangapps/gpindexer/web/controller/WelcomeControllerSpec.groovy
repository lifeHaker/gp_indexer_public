package com.orangapps.gpindexer.web.controller

import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.servlet.view.InternalResourceViewResolver
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
/**
 * Created by rurik on 01.02.15.
 */

class WelcomeControllerSpec extends Specification {

    MockMvc mockController;

    WelcomeController controller = new WelcomeController();

    def setup() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/views/jsp/");
        viewResolver.setSuffix(".jsp");
        mockController = MockMvcBuilders.standaloneSetup(controller).setViewResolvers(viewResolver).build()
    }

    def "WelcomeController's get urls response fine"() {
        expect:
        mockController.perform(get(url)).andExpect(result)

        where:
        url || result
        "/" || status().isOk()
    }

}
