package com.orangapps.gpindexer.web.controller

import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.servlet.view.InternalResourceViewResolver
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
/**
 * Created by rurik on 01.02.15.
 */

class StatisticsControllerSpec extends Specification {

    MockMvc mockController;

    StatisticsController controller = new StatisticsController();

    def setup() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/views/jsp/");
        viewResolver.setSuffix(".jsp");
        mockController = MockMvcBuilders.standaloneSetup(controller).setViewResolvers(viewResolver).build()
    }

    def "StatisticsController's get urls response fine"() {
        expect:
        mockController.perform(get(url)).andExpect(result)

        where:
        url || result
        "/get_statistics" || status().isOk()
    }

}
