package com.orangapps.gpindexer.web.controller
import com.orangapps.gpindexer.GenericGroovyContextLoader
import com.orangapps.gpindexer.fetcher.managment.SettingsManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.servlet.view.InternalResourceViewResolver
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
/**
 * Created by rurik on 01.02.15.
 */

@ContextConfiguration(locations = "classpath:web-test-config.groovy",
        loader = GenericGroovyContextLoader.class)
@TransactionConfiguration
class ManagementControllerSpec extends Specification {

    @Autowired
    private SettingsManager settingsManager;


    ManagementController controller = new ManagementController();

    MockMvc mockController;

    def setup() {
        controller.settingsManager = settingsManager
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/views/jsp/");
        viewResolver.setSuffix(".jsp");
        mockController = MockMvcBuilders.standaloneSetup(controller).setViewResolvers(viewResolver).build()
    }

    def "ManagementController's urls response fine"() {
        expect:
        mockController.perform(get(url)).andExpect(result)

        where:
        url                            || result
        "/start_gp_indexing"           || status().isOk()
        "/stop_apk_download"           || status().isOk()
        "/start_apk_download"          || status().isOk()
        "/start_gp_fetch"              || status().isOk()
        "/stop_images_download"        || status().isOk()
        "/start_images_download"       || status().isOk()
        "/start_gp_download"           || status().isOk()
        "/start_recovery"              || status().isOk()
        "/start_gp_fetch_and_download" || status().isOk()
        "/stop_gp_fetch_and_download"  || status().isOk()
        "/stop_gp_download"            || status().isOk()
        "/stop_all"                    || status().isOk()
    }

    def "Start download apk, fetch new ids, fetch new apps and recovery old apps works correctly"() {
        when:
        mockController.perform(get("/start_gp_indexing"))

        then:
        settingsManager.getIsApkDownloadActive();
        settingsManager.getIsGpDownloadActive();
        settingsManager.getIsGpFetchActive();
        settingsManager.isRecoveryActive();
    }

    def "Stop download apk, fetch new ids, fetch new apps and recovery old apps works correctly"() {
        when:
        mockController.perform(get("/stop_all"))

        then:
        !settingsManager.getIsApkDownloadActive();
        !settingsManager.getIsGpDownloadActive();
        !settingsManager.getIsGpFetchActive();
        !settingsManager.isRecoveryActive();
    }

}
